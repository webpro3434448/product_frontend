import type { Product } from '@/types/Product'
import http from './http'

function addProduct(product: Product) {
  return http.post('/product', product)
}

function updateProduct(product: Product) {
  return http.patch(`/product/${product.id}`, product)
}

function delProduct(product: Product) {
  return http.delete(`/product/${product.id}`)
}

function getProduct(id: number) {
  //fix
  return http.get(`/product/${id}`)
}

function getProducts() {
  return http.get('/product')
}

export default { addProduct, updateProduct, delProduct, getProduct, getProducts }
