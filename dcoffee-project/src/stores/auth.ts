import { ref, computed } from 'vue'
import { defineStore } from 'pinia'
import type { User } from '@/types/User'

export const useAuthStore = defineStore('auth', () => {
  const currentUser = ref<User[]>([
    {
      id: 1,
      email: 'nobita123@gmail.com',
      password: 'Pass@1234',
      fullName: 'โนบิตะ โนบิ',
      gender: 'male',
      roles: ['user']
    },
    {
      id: 2,
      email: 'misueri@gmail.com',
      password: 'Pass@1234',
      fullName: 'มิซึริ คันโรจิ',
      gender: 'female',
      roles: ['user']
    },
    {
      id: 3,
      email: 'chinege123@gmail.com',
      password: 'Pass@1234',
      fullName: 'ชินอีจิ คุโด้',
      gender: 'male',
      roles: ['user']
    },
    {
      id: 4,
      email: 'takechi@gmail.com',
      password: 'Pass@1234',
      fullName: 'ทาเคชิ โกดะ',
      gender: 'male',
      roles: ['user']
    },
    {
      id: 5,
      email: 'jaiko123@gmail.com',
      password: 'Pass@1234',
      fullName: 'ไจโกะ โกดะ',
      gender: 'female',
      roles: ['user']
    },
    {
      id: 6,
      email: 'nobisuke@gmail.com',
      password: 'Pass@1234',
      fullName: 'โนบิซุเกะ โนบิ',
      gender: 'male',
      roles: ['user']
    },
    {
      id: 7,
      email: 'chisooka123@gmail.com',
      password: 'Pass@1234',
      fullName: 'ชิซุกะ มินาโมโตะ',
      gender: 'female',
      roles: ['user']
    },
    {
      id: 8,
      email: 'dekisooyi@gmail.com',
      password: 'Pass@1234',
      fullName: 'เดคิซุงิ เดไซ',
      gender: 'male',
      roles: ['user']
    },
    {
      id: 9,
      email: 'sueneoe123@gmail.com',
      password: 'Pass@1234',
      fullName: 'ซึเนโอะ โฮเนคาว่า',
      gender: 'male',
      roles: ['user']
    },
    {
      id: 10,
      email: 'saywachi@gmail.com',
      password: 'Pass@1234',
      fullName: 'เซวาชิ โนบิ',
      gender: 'male',
      roles: ['user']
    }
  ])

  return { currentUser }
})
